# Unity Simple Third Person Camera
This is a simple third person camera script that can be extended and customized as needed by the user.
It support camera collision and avoidance of occluding obstacles.
The script include comments to explain the code and tooltips for the inspector.

## How to install
Simply download TpsCamera.cs and place it wherever you want in your project folder, it's a single class that you can access it as any other object.

## How to use
* Drop the script on your camera and set the target for the camera itself
* Set value of the exposed variables (they're mostly self explanatory but they're also explained in the code)
* The camera collides only with the selected layers in the "layers" variable so don't forget to set it up in the script and assign your choiche of collision layers to your GameObjects (they'll need a collider)

There's two main method exposed to be used:

### SetCameraVector
```
  SetCameraVector(Vector2 cameraVector)
```
It takes a single argument as a input and that's a Vector2 representing the direction of X and Y axis (it's better if you pass a normalized value)

#### SetZoomDirection (int zoom)
```
  SetZoomDirection (int zoom)
```
It also take a single argument as a input and that's a int, in this case it's better to pass -1,0,1 only as the velocity

## That's it
Test your game to tune the settings as best it suits your project or edit the script to extend it

## Author

* [**emmetiennegames**](https://gitlab.com/Emmetienne/)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
