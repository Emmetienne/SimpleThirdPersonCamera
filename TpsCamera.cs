 using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour {
    [Header("Basic gameobject & camera setup")]
    
    [Tooltip("Assign the target Gameobject to this")]
    public Transform cameraTarget;
    [Tooltip("Camera script")]
    public Camera cam;
    [Tooltip("Assign the transform of a Gameobject that'll be used as pivot point for the camera. For better debugging you can use a cube instead of an empty")]
    private Transform cameraPivot;
    [Tooltip("Define the offset from the center of the target Gameobject")]
    public Vector3 offset;
    [Tooltip("Define the orbital speed of the camera, this value is used both for X and Y rotations")]
    public float orbitalSpeed;
    
    [Header("Input")]
    private float zoomDirection;
    private Vector2 cVector;

    [Header("Finetuning camera setup")]
    public bool invertX;
    public bool invertY;

    [Tooltip("Set the velocity of the zoom")]
    public float zoomVelocity;

    [Tooltip("Starting value. This will be modified at runtime as part of the zoom method. An inspector hidden variable will store the value at Start() so it can be restored")]
    public float restRadius;
    
    private float startRadius;
    [Tooltip("Miminmum radius value. Be sure to test that there's no clipping with the target Gameobject")]
    public float minRadius;
    [Tooltip("Maximum radius value")]
    public float maxRadius;
    [Tooltip("Value used to place the camera away from the collision point. Be sure to test that there's no clipping with other object")]
    public float cameraNormalOffset;
    [Tooltip("Value used to soften the camera movement. Lower value lead to a softer but slower camera movements, viceversa higher value leat to a rougher and snappier camera movements")]
    public float cameraDampening;
    [Tooltip("Maximum upper angle of the camera. Avoid to set this value as 90 as it will lead north/south pole problems")]
    public float maxUpperAngle;
    [Tooltip("Maximum lower angle of the camera. Avoid to set this value as -90 as it will lead to north/south pole problems. Make sure to set a proper value to not cause clipping with the target Gameobject")]
    public float maxLowerAngle;
    
    private Vector3 pivotEuler;

    [Header("Raycast & collision")]
    public LayerMask layers;
    RaycastHit hit;
    private bool cameraCollision;
    private Vector3 desideredPos;
    private Vector3 lastPos;
    
    [Header("Debug options")]
    public bool camera_Debug;
   
	// Use this for initialization
	void Start () {
        startRadius = restRadius;
        LinkCamera();
        Placements();
    }

    // Update is called once per frame
    void Update () {
            
        //find camera euler's value based on the right stick or mouse input
        pivotEuler.x += cVector.y * orbitalSpeed * Time.deltaTime;
        pivotEuler.x = Mathf.Clamp(pivotEuler.x, maxLowerAngle, maxUpperAngle);
        pivotEuler.y += cVector.x * orbitalSpeed * Time.deltaTime;
        pivotEuler.z = 0f;
        
        Placements(); //call camera placement method      
        Zoom();
        Debug();        
    }   

    void FixedUpdate()
    {
        Avoidance();
    }

    void LateUpdate()
    {
        cameraPivot.transform.rotation = Quaternion.Euler(pivotEuler); //apply rotation to the pivot
       
        
        cam.transform.position = Vector3.Lerp (lastPos, desideredPos, Time.deltaTime * cameraDampening);
        cam.transform.LookAt(cameraPivot);
    }

    public void SetCameraVector(Vector2 cameraVector)
    {
        
        cVector = cameraVector;
        if (invertX == true) cVector.x = cVector.x * -1;
        if (invertY == true) cVector.y = cVector.y * -1;
    }

    public void SetZoomDirection (int zoom)
    {
        zoomDirection = zoom;
    }

    //probe if the camera is colliding with something
    void Avoidance()
    {
        if (Physics.Raycast(cameraPivot.transform.position, -cameraPivot.forward, out hit, -restRadius, layers))
        {
            cameraCollision = true;
        }
        else
        {
            cameraCollision = false;
        }
    }

    public void LinkCamera()
    {
         if (cameraPivot == null)
        {
            //create a cube to be used as pivot
            GameObject cameraPivotObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cameraPivotObject.name = "Camera Pivot";

            //disable the collider
            BoxCollider boxCollider = cameraPivotObject.GetComponent<BoxCollider>();
            boxCollider.enabled = false;

            //disable the rendering of the pivot
            Renderer cameraPivotMeshRenderer = cameraPivotObject.GetComponent<Renderer>();
            cameraPivotMeshRenderer.enabled = false;

            cameraPivot = cameraPivotObject.transform;

            cameraPivot.transform.position = cameraTarget.transform.position + new Vector3(offset.x, offset.y, offset.z);
        }

    }

    //find the correct camera placement in the world space (both when colliding and not colliding with something)
    void Placements()
    {
        cameraPivot.transform.position = cameraTarget.transform.position + new Vector3(offset.x, offset.y, offset.z);

        if (cameraCollision == false)
        {
            desideredPos = cameraPivot.transform.position + (cameraPivot.forward * (restRadius + cameraNormalOffset));          
        }
        else
        {
            desideredPos = hit.point + hit.normal * cameraNormalOffset;           
        }

        lastPos = cam.transform.position;
    }

    //self explanatory
    void Zoom()
    {
   
        if (zoomDirection != 0f)
        {
            restRadius = Mathf.Lerp(restRadius, restRadius - (zoomDirection * zoomVelocity), Time.deltaTime);
            restRadius = Mathf.Clamp(restRadius, maxRadius, minRadius);
        }

    }

    //this metod returns the normalized camera forward, it's not used in this script, but it can be used for character movements
    public Vector3 ForwardRight()
    {
        Vector3 cameraForward, camera_Right;
        cameraForward = new Vector3(cam.transform.forward.x, 0f, cam.transform.forward.z);
        cameraForward.Normalize();
        camera_Right = new Vector3(cam.transform.right.x, 0f, cam.transform.right.z);
        cameraForward.Normalize();
        return cameraForward;
    }


    //self explanatory
    void Debug()
    {
        if (camera_Debug == true)
        {

            UnityEngine.Debug.DrawRay(cameraPivot.transform.position, cameraPivot.transform.forward * 2f, Color.blue);
            UnityEngine.Debug.DrawRay(cam.transform.position, -cameraPivot.forward * (restRadius), Color.yellow);

            if (cameraCollision == true)
            {
                UnityEngine.Debug.DrawLine(cameraPivot.transform.position, hit.point, Color.green);
            }
            else
            {
                UnityEngine.Debug.DrawRay(cameraPivot.transform.position, cameraPivot.transform.forward * restRadius, Color.blue);
            }                 
                 
        }
        
    }


}
